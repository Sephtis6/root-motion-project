﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerCamera : MonoBehaviour {

    //variables used
    public Transform targetObjectTransform;
    public Vector3 offset;
    private Transform tf;

	// Use this for initialization
	void Start ()
    {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //position of camera changes to target objects current stransform + the offset
        //then has the camera look towards the objects position
        tf.position = targetObjectTransform.position + offset;
        tf.LookAt(targetObjectTransform.position);
	}
}
