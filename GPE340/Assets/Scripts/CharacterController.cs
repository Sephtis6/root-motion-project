﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {
    //vairables used
    public Pawn pawn;
    public Transform testObject; //for testing delete me later.

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //calls the rotation and movement void
        Rotation();
        Movement();
	}
    //void for rotation
    void Rotation()
    {
        //create a plane object
        Plane thePlane = new Plane(Vector3.up, pawn.tf.position);
        //raycast out the camera(at the mouse postion to the plane -- find the distance to the plane
        float distance;
        Ray theRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        thePlane.Raycast(theRay, out distance);
        //find a point on the ray that is "distance" down the ray
        Vector3 targetPoint = theRay.GetPoint(distance);
        //Temp: move a test object to that plane
        testObject.position = targetPoint;

        //rotate to look towards
        pawn.RotateTowards(targetPoint);
    }
    //void for movement
    void Movement()
    {
        //Move by useing vector 3 and creating a new movedirection using the inputs horizontal and vertical axis
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        //has the move direction modified by a clamp magnitude of movedirection and 1
        //then calls the pawns transform and inverse it's transform direction of the move direction
        moveDirection = Vector3.ClampMagnitude(moveDirection, 1.0f);
        moveDirection = pawn.tf.InverseTransformDirection(moveDirection);
        //calls the move void from pawn using the vecotr3 move direction
        pawn.Move(moveDirection);
    }
}
