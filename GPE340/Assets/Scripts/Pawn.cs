﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {
    //hides the anim and tf in inspector
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public Transform tf;
    //variables/floats used
    public float turnSpeed;
    public float moveSpeed;
    int jumpHash = Animator.StringToHash("Jump");
    int crouchHash = Animator.StringToHash("Crouch");
    

	// Use this for initialization
	void Start ()
    {
        //sets anim to animator and tf to transform
        anim = GetComponent<Animator>();
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if space pressed set the trigger allowing it to jump
		if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger(jumpHash);
        }
        //if x pressed set the trigger allowing it to crouch
        if (Input.GetKeyDown(KeyCode.X))
        {
            anim.SetTrigger(crouchHash);
        }
    }
    //void for the move function
    public void Move(Vector3 direction)
    {
        //has the animator set vertical and horizontal to the direction times the move speed
        anim.SetFloat("Vertical", direction.z * moveSpeed);
        anim.SetFloat("Horizontal", direction.x * moveSpeed);
    }
    //void to rotate towards
    public void RotateTowards(Vector3 targetPoint)
    {
        //creates a vector to look to that is the tartgetgiven in the header - the current tf
        //then has a quaternion called look rotation that uses the look rotation from quaternion with vector to look down and the tf.up
        //then it rotates the rtf using quaternion rotate towars with the look rotation and turnspeed and time.deltatime
        Vector3 vectorToLookDown = targetPoint - tf.position;
        Quaternion lookRotation = Quaternion.LookRotation(vectorToLookDown, tf.up);
        tf.rotation = Quaternion.RotateTowards(tf.rotation, lookRotation, turnSpeed * Time.deltaTime);
    }

}
